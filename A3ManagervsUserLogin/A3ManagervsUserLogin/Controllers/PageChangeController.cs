﻿using A3ManagervsUserLogin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace A3ManagervsUserLogin.Controllers
{
    public class PageChangeController : Controller
    {
        InvoiceContext db = new InvoiceContext();
        private int Idcounter = 0;
        // GET: PageChange
        public ActionResult Index()
        {
            Invoice i = new Invoice();
            i.InvoiceId = Idcounter;
            Idcounter++;
            ViewBag.fromWhere = "index";
            return View(i);
        }

        [HttpPost]           //no binding cus i want all the values that the user inputs
        [ValidateAntiForgeryToken]
        public ActionResult Index(Invoice invoce, String SubmitPaid)
        {
            //not sure if this will work yet bt lets see
            if (SubmitPaid == "Paid")
            {

            }

            Invoice i = new Invoice();
            //db.Invoices.Add(i);

            //i could use an enum for scalability but il use this cus i know this project will not be scaled up
            //as i am the one who s building it
            ViewBag.fromWhere = "index";
            return RedirectToAction("Index");
        }

        public ActionResult ToManagerAdd()
        {

            ViewBag.fromWhere = "managerAdd";
            Invoice i = new Invoice();
            i.InvoiceId = Idcounter;
            Idcounter++;

            return View(i);
        }

        [HttpPost]
        public ActionResult ToManagerAdd(Invoice invoice, String managerAdd)
        {
            Invoice i = new Invoice();
            
            if (managerAdd == "managerAdd")
            {

                return RedirectToAction("Receivable");

            }
            else

            return RedirectToAction("Index");
        }

        public ActionResult Receivable()
        {

            return View(db);
        }


    }
}