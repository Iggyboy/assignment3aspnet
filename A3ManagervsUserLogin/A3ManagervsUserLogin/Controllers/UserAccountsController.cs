﻿using A3ManagervsUserLogin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace A3ManagervsUserLogin.Controllers
{
    public class UserAccountsController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]  //user input comes here
        public ActionResult Index(User user, string loginType)
        {
            //validate the user password
            if (user.CheckPassword() == false)
            {
                //the password is incorrect, add an error to the model and return
                ModelState.AddModelError("Password", "Invalid password.");

                return View(user);
            }
            //check the button value that was passed
            if (loginType == "User Login")
            {
                if(user.CheckPassword() == false)
                {
                    //the password is wronge, i must add error in modelstate then return object back to begining screen
                    ModelState.AddModelError("Password", "Invalid password.");

                    return View(user);
                }
                else
                {
                    //save the user in the session 
                    Session[MvcApplication.SESSION_OBJ_USER] = user;
                    //i don't have to specify the word 'Controller'
                    return RedirectToAction("Index","UserAccounts");
                }
            }
            else if (loginType == "Manager Login")
            {
                if (user.UserName == "Amanda" || user.UserName == "Ray")
                {
                    //save the user in the session 
                    Session[MvcApplication.SESSION_OBJ_USER] = user;

                    return RedirectToAction("ToManagerAdd", "PageChange");
                }
                else
                {
                    //the user is not authorized for this role
                    TempData[MvcApplication.TEMP_OBJ_ERRMSG] = $"Unauthorized login attempt by {user.UserName} . The user is not a manager.";
                    return RedirectToAction("Index", "PageChange");
                }
            }
            else
            {
                //the user is not authorized for this role
                TempData[MvcApplication.TEMP_OBJ_ERRMSG] = $"Unauthorized login attempt by {user.UserName} . The user is not a manager.";
                return RedirectToAction("Index");
            }
       
            
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}