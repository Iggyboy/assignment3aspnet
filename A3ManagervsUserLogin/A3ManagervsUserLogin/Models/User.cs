﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace A3ManagervsUserLogin.Models
{
    //role of users
    public enum UserRole
    {
        User,
        Manager
    }

    public class User
    {
        //were gonna use theses to validate the user's inputed coresponding values
        [Required(ErrorMessage = "Please provide a user name.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter a password.")]
        public string Password { get; set; }

        public UserRole Role { get; set; }

        //sets the password to be opposite of username
        public bool CheckPassword()
        {
            //The requirements for the password is to be equal with the user name
            //in all lower-case and reversed
            return Password == new string(UserName.ToLower().Reverse().ToArray());
        }
    }
}