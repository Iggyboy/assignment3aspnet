﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
//import Entity so that i can use the super class DbContext
using System.Data.Entity;


namespace A3ManagervsUserLogin.Models
{
    public class InvoiceContext: DbContext
    {

        //pass name of subclass to super class DbContext
        public InvoiceContext() : base("name=InvoiceContext")
        {
        }
        //create the DbSet link to the sql database. Changes to this that are put in the
        //SQL database if SaveChanges() is called
        public DbSet<Invoice> Invoices { get; set; }
    }
}

