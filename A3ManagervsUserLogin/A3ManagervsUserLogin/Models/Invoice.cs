﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace A3ManagervsUserLogin.Models
{
    
    public enum Currency
    {
        CAD = 1,
        USD = 2,
        EUR = 3
    }

    public class Invoice
    {
        
        //set feild variables
        [Key]
        public int InvoiceId { get; set; }

        public string Name { get; set; }

        public int Address { get; set; }
      
        public string Shipment { get; set; }
       
        public string DueDate { get; set; }
       
        public string ProductName { get; set; }
  
        public int ProductQuality { get; set; }
        [Required(ErrorMessage = "Please specify the unit price for the product ordered")]
        [Range(0.0, double.MaxValue, ErrorMessage = "Please specify a positive number for the unit price")]
        public decimal UnitPrice { get; set; }

        public Currency Currency { get; set; }

        public const decimal TAX_RATE = 0.1m;

        /// <summary>
        /// Derived property that calculates and provides access to the subtotal. 
        /// </summary>
        public decimal SubTotal
        {
            get
            {
                return UnitPrice * ProductQuality;
            }
        }

        public decimal Tax { get { return this.SubTotal * TAX_RATE; } }

        public string Paid { get; set; }
    }
}