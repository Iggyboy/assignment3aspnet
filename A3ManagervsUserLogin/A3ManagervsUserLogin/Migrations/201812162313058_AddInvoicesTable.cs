namespace A3ManagervsUserLogin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvoicesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.Int(nullable: false),
                        Shipment = c.String(),
                        DueDate = c.String(),
                        ProductName = c.String(),
                        ProductQuality = c.Int(nullable: false),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Currency = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Invoices");
        }
    }
}
