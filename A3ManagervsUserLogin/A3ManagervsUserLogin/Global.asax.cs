﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace A3ManagervsUserLogin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //will be used to display error message and store most recent user
        public const string SESSION_OBJ_USER = "User";
        public const string TEMP_OBJ_ERRMSG = "Error";

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
